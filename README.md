# Backbase Weather App

![alt text](https://financialit.net/sites/default/files/mar15-thumb-backbase-logo-png_0.png "title")


**App Preview**: [https://bb-weatherapp.web.app](https://bb-weatherapp.web.app/)

## Overview
Weather app is a one stop application for all weather information about any city in the world,  get detailed and reliable weather forecast about your favorite city. 

## Tech Stack
-  Built with **Angular 7**
- styled in **SCSS**
- Hosted at **Firebase**
-  Served by **Open Weather Map**


## Architecture Overview
- Application is built with core angular framework, with zero dependencies on third party libraries and plugins.
- layout is developed with HTML & SCSS
- Data is pulled using weather service
- service and components are: 

### Service
**WeatherService** class exposes two important methods `getCityWeather(cityName:string)` & `getCityForecast(cityName:string)` responsible for getting the detailed city weather and forecast for the next days respectively.

### Component
**Dashboard :** Dashboard component loads city weather tiles with average temperature along with wind speed.
Dashboard component uses `<bb-city-tile>` component to render the tiles.

**CityDetailComponent :** renders forecast information for the following  hours. forecast information is displayed for every three hours in blocks, selecting each block displays detailed information on main screen.

## Getting Started
- Download / Clone Repo Code
- run `npm install`
- run `ng serve`
- application will be hosted on `localhost:4200`

## Screenshot

**Desktop View**

![alt text](https://firebasestorage.googleapis.com/v0/b/bb-weatherapp.appspot.com/o/desktop-full.PNG?alt=media&token=11562898-1741-48ba-84cb-65ed5b34999b "title")


**Desktop Detail View**

![alt text](https://firebasestorage.googleapis.com/v0/b/bb-weatherapp.appspot.com/o/desktop.PNG?alt=media&token=558bf8a9-d54f-45f4-becf-488fd397632d "title")


**Mobile View**

![alt text](https://firebasestorage.googleapis.com/v0/b/bb-weatherapp.appspot.com/o/mobile.PNG?alt=media&token=d386b22d-e865-4752-9a18-0d33060a47bb "title")



**Mobile Detail View**

![alt text](https://firebasestorage.googleapis.com/v0/b/bb-weatherapp.appspot.com/o/mobile-detail.PNG?alt=media&token=70546541-d861-47cc-b540-6d80d0f35d87 "title")